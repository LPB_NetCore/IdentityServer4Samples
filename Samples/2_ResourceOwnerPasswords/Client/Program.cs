﻿using System;
using System.Net.Http;
using IdentityModel;
using IdentityModel.Client;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new HttpClient();
            var disdoc = client.GetDiscoveryDocumentAsync("http://localhost:5000").Result;
            if (disdoc.IsError)
            {
                Console.WriteLine(disdoc.Error);
            }

            var tokenResponse = client.RequestPasswordTokenAsync(new PasswordTokenRequest()
            {
                Address = disdoc.TokenEndpoint,
                ClientId = "pwdclient",
                ClientSecret = "secret",
                Scope = "api",

                UserName = "Test1",
                Password = "123456"

            }).Result;

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
            }
            else
            {
                Console.WriteLine(tokenResponse.Json);
            }

            HttpClient httpClient = new HttpClient();
            httpClient.SetBearerToken(tokenResponse.AccessToken);
            var response = httpClient.GetAsync("http://localhost:5001/api/values").Result;
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            }

            Console.ReadLine();
        }
    }
}
